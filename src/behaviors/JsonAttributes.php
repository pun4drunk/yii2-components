<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\behaviors;

/**
 * Description of SerializedAttributes
 *
 * @author Vladislav
 */
class JsonAttributes extends TransformedAttributes {
    
    public function init() {
        
        $this->processCallback = function($value) {
            return json_encode($value);
        };
        
        $this->unprocessCallback = function($value) {
            return json_decode($value);
        };

        parent::init();
    }
    
}
 
