<?php


namespace common\components\behaviors;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use Yii;

class ControllersBehavior extends Behavior {
    
    protected $controllers = [];
    public $map = [];
    
    public function get($alias) {
        if (!isset($this->controllers[$alias])) {
            $id = ArrayHelper::getValue($this->map, $alias, $alias);
            list($controller, $route) = Yii::$app->createController($id);
            $this->controllers[$alias] = $controller;
        }
        return $this->controllers[$alias];
    }
    
}
