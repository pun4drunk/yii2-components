<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\behaviors;

/**
 * Description of ModelStack
 *
 * @author Vladislav
 */
class ModelStack extends \yii\base\Behavior {
    
    public $models = [];
    
    public function __call($name, $params) {
        $result = [];
        foreach ($this->models as $model) {
            $result = array_merge($result, call_user_func_array([$model, $name], $params));
        }
        
        return $result;
    }
    
    public function getModel($name = NULL) {
        
        $result = reset($this->models);
        
        if (!is_null($name)) {
            if (!isset($this->models[$name])) {
                throw new \yii\base\InvalidCallException("Model defined but not set: $name: ".__METHOD__);
            }
            $result = $this->models[$name];
        }
        
        return $result;
    }
    
}
