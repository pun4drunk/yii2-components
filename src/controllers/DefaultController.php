<?php

namespace common\components\controllers;

use yii\filters\VerbFilter;
use Yii;
use common\components\models\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\base\Event;
use yii\web\View;
use common\components\actions\IndexAction;
use common\components\actions\CreateAction;

class DefaultController extends WebController
{
    public $modelClass;
    public $modelFormClass;
    public $modelSearchClass;
    public $editableModelClass;
    
    public function init() {
        parent::init();
        if (!$this->modelFormClass) {
            $this->modelFormClass = $this->modelClass;
        }
        
        if (!$this->editableModelClass) {
            $this->editableModelClass = $this->modelClass;
        }
    }
    
    public function actionDetails($id) {
        return $this->actionUpdate($id);
    }
    
    public function actions() {
        return array_merge(parent::actions(), [
            'index' => [
                'class' => IndexAction::className(),
                'searchModelClass' => $this->modelSearchClass,
                'searchAttributesCallback' => function(\yii\base\Model $searchModel) {
                    return [];
                },
            ],
            'create' => [
                'class' => CreateAction::className(),
                'modelClass' => $this->modelFormClass,
                'createAttributesCallback' => function(\yii\base\Model $model) {
                    return [];
                },
            ],
                        
        ]);
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    
                ],
            ],
        ];
    }
    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, true);
        
        $this->performAjaxValidation($model);
        
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->save()) {
                if (!$returnUrl = filter_input(INPUT_POST, 'returnUrl' , FILTER_SANITIZE_URL)) {
                    $returnUrl = ['index'];
                }
                return $this->redirect($returnUrl);
            } else {
                
                throw new \yii\web\ServerErrorHttpException(Yii::t('app', 'Failed to save model'));
            }

        } 
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    protected function delete($id) {
        $model = $this->findModel($id);
        if (!$result = $model->delete()) {
            throw new \yii\base\Exception("Delete error: ".print_r($model->getErrors(), true));
        }
        return $result;
    }
    
    public function actionDelete($id)
    {
        try {
            $this->delete($id);
        }catch (\Exception $e) {
            throw($e);
            Yii::$app->getSession()->setFlash('error', [
                'type' => 'error',
                'duration' => 5000,
                'message' => $e->getMessage(),
                'title' => Yii::t('app', 'Delete Error'),
            ]);
        }
        
        if (!$returnUrl = Yii::$app->request->referrer) {
            $returnUrl = ['index'];
        }
        
        return $this->redirect($returnUrl);
    }
    
    public function performAjaxValidation(\yii\base\Model $model)
    {
        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                echo json_encode(ActiveForm::validate($model));
                Yii::$app->end();
            }
        }
    }
    
    public function findModel($id) {
        return $this->loadModel($id);
    }
    
    public function loadModel($id, $edit = false, $modelClass = NULL) {
        
        if (is_null($modelClass)) {
            $modelClass = $edit ? $this->modelFormClass : $this->modelClass;
        }
        
        if (!$model = $modelClass::findOne($id)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        return $model;
    }
    
    
    public function actionRowDetails() {
        
        try {
        
            Event::on(View::className(), View::EVENT_AFTER_RENDER, function ($e) {
                $e->sender->assetBundles = [];
            });
            
            return $this->renderAjax('_row_details', [
                'model'=>$this->findModel(Yii::$app->request->post('expandRowKey')),
            ]);
        } catch (\Exception $e) {
            return '<div class="alert alert-danger">'.  \yii\helpers\Html::encode($e->getMessage()).'</div>';
        }
    }
    
    
    public function checkAccess($verb) {
        $params = array();
        if ('update' === $verb) {
            if ($id = Yii::$app->request->getQueryParam('id')) {
                $params['model'] = $this->findModel($id);
            }
        }
        return $this->user->can("{$verb}Item", $params);
    }
    
    protected function editModel($id) {
        
        $className = $this->editableModelClass;
        $output = "";
        
        if ($model = $this->loadModel($id, true, $className)) {
            // store a default json response as desired by editable
            $out = Json::encode(['output'=>'', 'message'=>'']);
            $class = new \ReflectionClass($className);
            $classShort = $class->getShortName();
        
            if ($data = Yii::$app->request->post($classShort)) {
                
                $posted = reset($data);
                //fallback if only one model is received
                if (!is_array($posted)) {
                    $posted = $data;
                }

                if (count($posted) > 1) {
                    throw new \yii\web\BadRequestHttpException("Editable can only accept a single parameter");
                }

                $output = $this->updateEditable($model, [$classShort => $posted]);
            }
        } 

        // return ajax json encoded response and exit
        echo Json::encode(['output'=>$output, 'message'=>'']);
        return;
    }
    
    protected function updateEditable($model, $attributes) {
        $output = "";
        
        // load model like any single model validation
        if ($model->load($attributes) && $model->save()) {
            $attribute = key(current($attributes));
            
            if (method_exists($model, $nameGetter = "get".ucfirst($attribute).'Name')){
                $output = $model->$nameGetter();
            }
        }
        
        return $output;
    }
    
}
