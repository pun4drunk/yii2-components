<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\models;

use yii\base\Model;
use Yii;

/**
 * UploadForm is the model behind the upload form.
 */
abstract class UploadForm extends Model
{
    public $files = [];
    public $isAdmin = false;
    protected $maxFiles = 1;
    protected $maxFilesAdmin = 99;
    
    public function getMaxFiles() {
        return $this->isAdmin ? $this->maxFilesAdmin : $this->maxFiles;
    }
    
    public function rules() {
        return [
            'files' => [['files'], 'each', 'rule' =>  ['file', 'skipOnEmpty' => false, 
                'maxFiles' => $this->getMaxFiles(),
            ]],
        ];
    }
    
    public function save($runValidation = true, $attributeNames = NULL) {
        
        if ($runValidation && !$this->validate()) {
            return false;
        }
            
        foreach ($this->files as $file) {
            $this->saveSingle($file);
        }
        
        return !count($this->errors);
    }
    
    abstract protected function saveSingle($file);
}
