<?php

namespace common\components\models;

class ActiveQuery extends \yii\db\ActiveQuery {
    
    public function excludeId($id, $tableName = "") {
        if (!is_null($id)) {
            $field = $tableName ? "[[$tableName.id]]" : "id";
            $this->andWhere("$field <> :excludeId", [':excludeId' => $id]);
        }
        return $this;
    }
    
    public function byId($id, $tableName = "") {
        if (!is_null($id)) {
            $field = $tableName ? "[[$tableName.id]]" : "id";
            $this->andWhere([
                $field => $id,
            ]);
        }
        return $this;
    }
    
    public function excludeIds(array $ids, $tableName = "") {
        if ($ids) {
            $field = $tableName ? "[[$tableName.id]]" : "id";
            $this->andWhere(['not in', $field, $ids]);
        }
        
        return $this;
    }
}
