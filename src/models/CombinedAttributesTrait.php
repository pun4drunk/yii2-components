<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\models;

trait CombinedAttributesTrait {

    protected $_attributes = [];

    public function initAttributes(array $attributes) {
        $this->_attributes = $attributes;
    }
    
    public function __get($name) {
        
        if (array_key_exists($name, $this->_attributes)) {
            return $this->_attributes[$name];
        } else {
            return parent::__get($name);
        }
    }
    
    public function __set($name, $value) {
        
        if (array_key_exists($name, $this->_attributes)) {
            $this->_attributes[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }
}
