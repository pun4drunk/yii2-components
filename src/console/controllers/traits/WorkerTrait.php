<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace components\console\controllers\traits;

/**
 * Description of WorkerTrait
 *
 * @author Vladislav
 */
trait WorkerTrait {
    
    
    public function reset() {
        return $this;
    }

    public function work($taskData, $routingKey = NULL) {
        
        $task = array_merge([
            'method' => NULL,
            'args' => NULL,
        ], json_decode($taskData, true));
        
        return 0 === call_user_func_array([$this, "action".ucfirst($task['method'])], $task['args']);
    }
    
}
