<?php

namespace common\components\helpers;
use kato\getid3\Yii2GetID3;

abstract class FileInfo {
    
    public static function get($filename) {
        
        if (!file_exists($filename)) {
            throw new \yii\base\Exception("File $filename does not exist");
        }
        
        $getID3 = new Yii2GetID3();
            
        return $getID3->getData($filename);
    }
}
