<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\helpers;
use yii\helpers\ArrayHelper;

/**
 * Description of EnumHelper
 *
 * @author Vladislav
 */
abstract class EnumHelper {

    public static function getOptions() {
        return [];
    }
    
    public static function getPluralOptions() {
        
    }
    
    public static function all($plural = false) {
        return array_keys($plural ? static::getPluralOptions() : static::getOptions());
    }
    
    public static function getLabel($value, $plural = false, $default = NULL) {
        return ArrayHelper::getValue($plural ? static::getPluralOptions() : static::getOptions(), $value, $default);
    }
    
    
    
}
