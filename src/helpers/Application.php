<?php

namespace common\components\helpers;
use Yii;

abstract class Application {
    
    const BACKEND   = 'app-backend';
    const FRONTEND  = 'app-frontend';
    const CONSOLE   = 'app-console';
    const REST      = 'app-rest';
    
    
    public static function isAdmin($default = false) {
        
        switch (static::getId()) {
            
            case static::BACKEND:
                $result = Yii::$app->user->identity && Yii::$app->user->identity->isAdmin;
                break;
                
            case static::CONSOLE:
                $result = true;
                break;
            
            case static::FRONTEND:
            case static::REST:
                $result = false;
                break;
            
            default:
                $result = $default;
                break;
        }
        
        return $result;
         
    }
    
    public static function isWeb() {
        return in_array(static::getId(), [
            static::BACKEND, 
            static::FRONTEND,
        ]);
    }
    
    public static function getId() {
        return Yii::$app->id;
    }
    
    public static function getLanguageCode() {
        $language = Yii::$app->language;
        $code = substr($language, 0, strpos($language, '-'));
        return $code;
    }
    
}