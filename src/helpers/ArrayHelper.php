<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\helpers;
use yii\base\Model;
use yii\helpers\ArrayHelper as ArrayHelperBase;

/**
 * Description of ValidationErrors
 *
 * @author Vladislav
 */
abstract class ArrayHelper extends ArrayHelperBase {
    
    public static function flatten(array $array) {
        $result = [];
        
        foreach ($array as $key => $value) {
            if (is_array(reset($value))) {//detect multi-dimensional
                $value = static::flatten($value);
            }
            $result = $result + $value;
        }
        
        return $result;
    }
    
    public static function mapKeys(array $array, $from) {
        $result = [];
        foreach ($array as $element) {
            $result[static::getValue($element, $from)] = $element;
        }
        return $result;
    }
    
}
