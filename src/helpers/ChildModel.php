<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\helpers;
use common\components\models\ChildModelInterface;

/**
 * Description of ChildModel
 *
 * @author Vladislav
 */
abstract class ChildModel {
    
    public static function triggerParentsUpdate(ChildModelInterface $model, $attribute = 'updated_at') {
        //trigger parents update
        while($parent = $model->parent) {
            $model = $parent;
            $model->updateAttributes([
                $attribute => time(),
            ]);
        }
        return true;
    }
}
