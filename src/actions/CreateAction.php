<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\actions;
use common\components\controllers\DefaultController;
use Yii;

/**
 * Description of CreateAction
 *
 * @author Vladislav
 */
class CreateAction extends \yii\base\Action {
    
    /**
     * Default create attributes callback
     * @var callable 
     */
    public $createAttributesCallback;
    
    
    /**
     * Model class
     * @var string 
     */
    public $modelClass;
    
    public $viewName = 'create';
    
    public function init() {
        
        parent::init();
        
        if (!$this->modelClass) {
            throw new \Exception("Model class should be defined for ".get_class($this));
        }
        
        if (!$this->controller instanceof DefaultController) {
            throw new \Exception(get_class($this)." can only be used with ".DefaultController::className());
        }
        
        if (!is_null($this->createAttributesCallback)) {
            if (!is_callable($this->createAttributesCallback)) {
                throw new \Exception("createAttributesCallback can only be callable in ".get_class($this));
            }
        }
    }
    
    public function run() {
        
        $model = $this->newModel();
        
        if (is_callable($this->createAttributesCallback)) {
            if ($createAttributes = call_user_func($this->createAttributesCallback, $model)) {
                $model->setAttributes($createAttributes);
            }
        }
        
        $this->controller->performAjaxValidation($model);
        
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->save()) {
                if (!$returnUrl = filter_input(INPUT_POST, 'returnUrl' , FILTER_SANITIZE_URL)) {
                    $returnUrl = ['index'];
                }
                $this->afterSave($model);
                return $this->controller->redirect($returnUrl);
            } else {
                throw new \yii\web\ServerErrorHttpException(Yii::t('app', 'Failed to save model'));
            }
            
        } else {
            
            return $this->controller->render($this->viewName, [
                'model' => $model,
            ]);
        }
    }
    
    protected function newModel() {
        return new $this->modelClass;
    }
    
    protected function afterSave(\yii\base\Model &$model) {}
}
