<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\actions;
use yii\base\Action;
use Yii;
/**
 * Description of SafeDeleteAction
 *
 * @author Vladislav
 */
class SafeDeleteAction extends Action {
    
    public $deletedAttribute = 'deleted_at';
    public $deletedByAttribute = 'deleted_by';
    public $undeleteValue = 0;
    
    public $returnUrl;
    public $callback;
    
    public function init() {
        parent::init();
        if (is_null($this->returnUrl)) {
            $this->returnUrl = Yii::$app->request->referrer;
        }
    }
    
    public function run($id) {
        
        $model = $this->controller->findModel($id);
        $value = $model->{$this->deletedAttribute};
        
        $model->updateAttributes([
            $this->deletedAttribute     => $value ? $this->undeleteValue : $this->deleteValue(),
            $this->deletedByAttribute   => $value ? NULL : Yii::$app->user->id,
        ]);
        
        if (is_callable($this->callback)) {
            call_user_func($this->callback, $model);
        }
        
        $this->controller->redirect($this->returnUrl);
        return true;
    }
    
    public function deleteValue() {
        return time();
    }
}
