<?php

namespace common\components\actions;
use yii\base\Action;
use Yii;


class InvokeConsoleCommandAction extends Action {
    
    public $returnUrl;
    public $cmd;
    public $sleep = 0;
    
    public function init() {
        parent::init();
        if (is_null($this->cmd)) {
            //@TODO: clarify exception class
            throw new \Exception("Console command is not specified for ".get_class($this));
        }
        
        if (!is_callable($this->returnUrl)) {
            //@TODO: clarify exception class
            throw new \Exception("Return URL is not specified for ".get_class($this));
        }
    }
    
    public function run($id) {
        $cmd = sprintf($this->cmd, $id);
        Yii::$app->consoleRunner->run($cmd);
        $url = call_user_func($this->returnUrl, $id);
        return $this->controller->redirect($url);
    }
}
