<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\actions;
use yii\base\Action;
use Yii;

class UpdateModelAttributeValueAction extends Action {
    
    public $accessVerb = 'update';
    public $attribute;
    public $value;
    public $test;
    public $exceptionCode = 400;
    public $error;
    public $returnUrl;
    public $getModel;
    public $redirect = true;
    
    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        
        if (is_null($this->error)) {
            $this->error = Yii::t('app', 'Invalid request');
        }
        
        if (is_null($this->returnUrl)) {
            $this->returnUrl = Yii::$app->request->referrer;
        }
        
        if (is_null($this->getModel)) {
            $action = $this;
            $this->getModel = function($id) use ($action) {
                return $action->controller->findModel($id);
            };
        }
    }
    
    
    public function run($id) {
        
        if ($this->accessVerb) {
            $this->controller->checkAccess($this->accessVerb);
        }
        
        $model = call_user_func($this->getModel, $id);
        
        if (!is_null($this->test)) {
            if (is_callable($this->test)) {
                $test = call_user_func($this->test, $model);
            }
            
            if (!$test) {
                throw new \yii\web\HttpException($this->exceptionCode, $this->error);
            }
        }
        
        $value = strlen($this->value) ? $this->value : Yii::$app->request->post('value');
        $model->{$this->attribute} = $value;
        
        if (!$model->save()) {
            throw new \yii\web\ServerErrorHttpException(Yii::t('app', 'Failed to update model'));
        }
        
        
        $result = true;
        
        if ($this->redirect) {
            $result = $this->controller->redirect($this->returnUrl);
        }
        
        return $result;
    }
}
