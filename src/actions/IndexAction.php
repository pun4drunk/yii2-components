<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\actions;
use common\components\controllers\DefaultController;
use Yii;
/**
 * Description of IndexAction
 *
 * @author Vladislav
 */
class IndexAction extends \yii\base\Action {
    
    /**
     * Search Model class
     * @var string 
     */
    public $searchModelClass;
    
    /**
     * Attributes callback
     * @var callable 
     */
    public $searchAttributesCallback;
    
    /**
     *
     * @var DefaultController 
     */
    public $controller;
    
    /**
     * View name to render
     * @var string 
     */
    public $viewName = 'index';
    
    public function init() {
        
        parent::init();
        
        if (!$this->searchModelClass) {
            throw new \Exception("Search model class should be defined for ".get_class($this));
        }
        
        if (!$this->controller instanceof DefaultController) {
            throw new \Exception(get_class($this)." can only be used with ".DefaultController::className());
        }
        
        if (!is_null($this->searchAttributesCallback)) {
            if (!is_callable($this->searchAttributesCallback)) {
                throw new \Exception("searchAttributesCallback can only be callable in ".get_class($this));
            }
        }
    }
    
    public function run() {
        
        if (Yii::$app->request->post('hasEditable')) {
            return $this->controller->editModel(Yii::$app->request->post('editableKey'));
        }
        
        $searchModel = new $this->searchModelClass;
        
        if (is_callable($this->searchAttributesCallback)) {
            if ($searchAttributes = call_user_func($this->searchAttributesCallback, $searchModel)) {
                $searchModel->setAttributes($searchAttributes);
            }
        }
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->controller->render($this->viewName, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
}
