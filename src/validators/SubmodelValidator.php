<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace common\components\Validators;
use yii\validators\Validator;
use Yii;

/**
 * Description of SubmodelValidator
 *
 * @author Vladislav
 */
class SubmodelValidator extends Validator {
    
    public $submodelClass;
    public $attributes = [];
    
    protected $submodel;
    
    
    public function validateAttribute($model, $attribute)
    {
        if (!$this->validateValue($model->$attribute)) {
            foreach ($submodel->errors as $error) {
                $this->addError($model, $attribute, $error);
            }    
        }
        
    }
    
    public function validateValue($value) {
        
        $result = NULL;
        
        $this->submodel = new $this->submodelClass;
        $attributes = array_merge($this->attributes, $value);
        
        $this->submodel->load($attributes, "");
        if (!$this->submodel->validate()) {
            
            $errors = $this->submodel->errors;
            $errors = reset($errors);
            $error  = reset($errors);
            
            $result = [Yii::t('app', "Submodel {$this->submodel->title} is invalid: {error}", [
                'error' => $error,
            ]), []];
        }
        
        return $result;
    }
    
    public function getErrors() {
        return $this->submodel->getErrors();
    }
    
}
